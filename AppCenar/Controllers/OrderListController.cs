﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppCenar.Models;

namespace AppCenar.Controllers
{
    public class OrderListController : Controller
    {

        private UsersContext db = new UsersContext();

        //
        // GET: /OrderList/
        public ActionResult Index(int idRestaurant)
        {
            var detalles = db.RestaurantDetails.Where(x => x.IdRestaurant == idRestaurant).ToList();

            return View(detalles);
        }

    }
}
