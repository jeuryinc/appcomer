﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppCenar.Models;

namespace AppCenar.Controllers
{
    public class RestaurantDetailsController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /RestaurantDetails/

        public ActionResult Index()
        {
            return View(db.RestaurantDetails.ToList());
        }

        //
        // GET: /RestaurantDetails/Details/5

        public ActionResult Details(int id = 0)
        {
            RestaurantDetails restaurantdetails = db.RestaurantDetails.Find(id);
            if (restaurantdetails == null)
            {
                return HttpNotFound();
            }
            return View(restaurantdetails);
        }

        //
        // GET: /RestaurantDetails/Create

        public ActionResult Create(int idRestaurant)
        {
            var restaurant = new RestaurantDetails
            {
                IdRestaurant = idRestaurant
            };

            ViewBag.Categorys = new SelectList(db.Category.ToList(), "IdCategory", "Name");

            return View(restaurant);
        }

        //
        // POST: /RestaurantDetails/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RestaurantDetails restaurantdetails)
        {
            if (ModelState.IsValid)
            {
                db.RestaurantDetails.Add(restaurantdetails);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(restaurantdetails);
        }

        //
        // GET: /RestaurantDetails/Edit/5

        public ActionResult Edit(int id = 0)
        {
            RestaurantDetails restaurantdetails = db.RestaurantDetails.Find(id);
            if (restaurantdetails == null)
            {
                return HttpNotFound();
            }
            return View(restaurantdetails);
        }

        //
        // POST: /RestaurantDetails/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RestaurantDetails restaurantdetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(restaurantdetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(restaurantdetails);
        }

        //
        // GET: /RestaurantDetails/Delete/5

        public ActionResult Delete(int id = 0)
        {
            RestaurantDetails restaurantdetails = db.RestaurantDetails.Find(id);
            if (restaurantdetails == null)
            {
                return HttpNotFound();
            }
            return View(restaurantdetails);
        }

        //
        // POST: /RestaurantDetails/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RestaurantDetails restaurantdetails = db.RestaurantDetails.Find(id);
            db.RestaurantDetails.Remove(restaurantdetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}