﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppCenar.Models
{
    public class Location
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
    }
}