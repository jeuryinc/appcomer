namespace AppCenar.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryAndDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        IdCategory = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.IdCategory);
            
            CreateTable(
                "dbo.RestaurantDetails",
                c => new
                    {
                        IdRestaurantDetails = c.Int(nullable: false, identity: true),
                        IdRestaurant = c.Int(nullable: false),
                        IdCategory = c.Int(nullable: false),
                        Name = c.String(),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.IdRestaurantDetails);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RestaurantDetails");
            DropTable("dbo.Category");
        }
    }
}
