﻿var x = document.getElementById("demo");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
           

            showPosition(position);

          //  alert("latitud: " + latitude + " longitud: " + "   aquesta es la precisio en metres  :  " + accuracy);
           
        }, function error(msg) {
            alert('Please enable your GPS position future.');

        }, { maximumAge: 600000, timeout: 5000, enableHighAccuracy: true });

    } else {
        alert("Geolocation API is not supported in your browser.");
    }

}

function showPosition(position) {
    var latlondata = position.coords.latitude + "," + position.coords.longitude;
    

    var win = window.open("http://maps.googleapis.com/maps/api/staticmap?center="
        + latlondata + "&zoom=14&size=400x300&sensor=false", '_blank');
    win.focus();

}
function showError(error) {
    if (error.code == 1) {
        x.innerHTML = "User denied the request for Geolocation."
    }
    else if (err.code == 2) {
        x.innerHTML = "Location information is unavailable."
    }
    else if (err.code == 3) {
        x.innerHTML = "The request to get user location timed out."
    }
    else {
        x.innerHTML = "An unknown error occurred."
    }
}