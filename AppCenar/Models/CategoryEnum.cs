﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppCenar.Models
{
    public enum CategoryEnum
    {
        Pizza = 1,
        Hamburguesas = 2,
        Postres = 5,
        Sandwiches = 7,
        Helados = 8
    }
}