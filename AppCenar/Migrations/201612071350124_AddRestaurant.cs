namespace AppCenar.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRestaurant : DbMigration
    {
        public override void Up()
        {
         
            CreateTable(
                "dbo.Restaurant",
                c => new
                    {
                        IdRestaurant = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Presupuesto = c.String(),
                        Picture = c.String(),
                        Rating = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.IdRestaurant);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Restaurant");
        }
    }
}
